#!/usr/bin/env python

from despotic import cloud as _cloud
import numpy as _np
import emcee as _emcee
import triangle as _triangle
import sys as _sys

__author__ = "George C. Privon"

# physical conditions to be reported:
# amp, nH, colDen, sigmaNT, Tg (keep everything else the same)
# Ranges of (log values):
prange=[(-5,0.2), (1,7), (18,26), (5,8), (0.69897,2)]


def rosebud(SLED,SLEDerr,TB=True,ncomp=1,plotfile=None,nthreads=1,VERBOSE=False,nwalkers=20,nburn=100,nchain=200,SLEDplot=None,truths=None):
    """
    rosebud is a MCMC code to fit spectral line SLEDs

    Arguments:
        SLED: Dictionary of SLED values. Key should be the species,
        SLEDerr: Dictionary of SLED error values. Key should be the species
        TB: If True (default), SLED is in brightness temperature.
        ncomp: Number of emitting components to fit (only 1 supported, 
            currently)
        plotfile: name (incl suffix) of file to save corner plot
        nthreads: number of threads to use
        VERBOSE: If true print periodic status messages
        nwalkers: number of walkers to use
        nburn: Number of iterations to burn.
        nchain: length of chain
        SLEDplot: If not none, plot the observed SLED(S) along with the results
            of the fitting. (NOT YET FULLY IMPLEMENTED)
        truths: If present, the true values of the parameters. Length of array 
            must be ncomp*5.

    Returns:
        fitparams: list of tuples corresponding to the preferred parameter 
            values (marginalized over the other values). Each tuple has 3 parts:
            50th percentile, upper delta, lower delta. Currently computed using 
            16th, 50th, and 84th percentiles (approx 1-sigma if the distribution
            is Gaussian).

    Requires (all can be installed via pip):
        triangle_plot
        matplotlib
        numpy
        emcee
        despotic

    Other:
        Code checks the first entry of each SLED. If it's equal to 1, the SLED 
        is assumed to be normalized to the (1-0) transition. Empty values should
        not be omitted and should be noted by np.nan's.

    Author;
        George C. Privon
        March 2014

    """

    if truths is not None:
        if not(len(truths)==ncomp*5):
            truths=None
            _sys.stderr.write("truths value incompatible with requested number of components. Not plotting truths.\n\n")

    if VERBOSE: _sys.stderr.write("Generating random parameters for walkers.\n")
    # generate nwalkers initial conditions into a ncompxnwalkersx5 array
    p0=_np.array([[0,   # log(amplitude)
        (_np.random.rand()*(prange[1][1]-prange[1][0])+prange[1][0]),    # log nH
        (_np.random.rand()*(prange[2][1]-prange[2][0])+prange[2][0]),    # log NH
        (_np.random.rand()*(prange[3][1]-prange[3][0])+prange[3][0]),    # log sigmaNT
        (_np.random.rand()*(prange[4][1]-prange[4][0])+prange[4][0])]*ncomp]) # log Tg
    for j in range(nwalkers-1):
        p0=_np.concatenate((p0,_np.array([[0,
            (_np.random.rand()*(prange[1][1]-prange[1][0])+prange[1][0]),    # log nH
            (_np.random.rand()*(prange[2][1]-prange[2][0])+prange[2][0]),    # log NH
            (_np.random.rand()*(prange[3][1]-prange[3][0])+prange[3][0]),    # log sigmaNT
            (_np.random.rand()*(prange[4][1]-prange[4][0])+prange[4][0])]*ncomp])),axis=0) # log Tg

    sampler=_emcee.EnsembleSampler(nwalkers, p0.shape[1], SLEDprob, args=[SLED,SLEDerr,TB,ncomp],threads=nthreads)

    # burn in
    if VERBOSE: _sys.stderr.write("Burning in.\n")
    pos, prob, state = sampler.run_mcmc(p0, nburn)
    sampler.reset()

    if VERBOSE: _sys.stderr.write("Running MCMC.\n")
    sampler.run_mcmc(pos, nchain)
    samples = sampler.chain.reshape((-1,p0.shape[1]))

    if VERBOSE: _sys.stderr.write("Generating triangle plot.\n")
    fig = _triangle.corner(samples,
        labels=ncomp*["log Amp","log $n_H$","log $N_H$","log $\sigma_{NT}$","log $T_{gas}$"],
        extents=ncomp*prange,
        quantiles=[0.16,0.5,0.84],
        truths=truths)

    print("Mean acceptance fraction: {0:.3f}"
                                .format(_np.mean(sampler.acceptance_fraction)))

    # compute the parameter results with some confidence intervals (16,50,84th
    # percentile). These will be written to stderr and also returned
    samples[:,2]=_np.exp(samples[:,2])
    fitparams=map(lambda v: (v[1], v[2]-v[1], v[1]-v[0]),
    zip(*_np.percentile(samples, [16, 50, 84],axis=0)))

    print fitparams

    if plotfile is None:
        fig.savefig("test.png")
    else:
        fig.savefig(plotfile)
        # save samples
#        f=open(plotfile.split('.')[0]+'.dat','w')
#        for sample in samples:
#            position = sample[0]
#            for k in range(position.shape[0]):
#                f.write("{0:4d} {1:s}\n".format(k, " ".join(position[k])))
#        f.close()

    if not(SLEDplot is None):
        if VERBOSE: _sys.stderr.write("Plotting SLED and model fit to "+SLEDplot+"\n")
        import matplotlib.pyplot as plt
        plt.figure()
        plt.xlabel('J upper',fontsize='x-large')
        plt.xlim([0,10])
        plt.ylim([0.5*_np.min(SLED[SLED.keys()[0]][0:11]),1.5*_np.max(SLED[SLED.keys()[0]][0:11])])
        if (SLED[SLED.keys()[0]][0]-1) < 1e-5: # assume SLED is normalized, 1-0
            plt.ylabel('L$_{line (J-J-1)}$/L$_{line (1-0)}$',fontsize='x-large')
        else:
            plt.ylabel('L$_{line (J-J-1)}$',fontsize='x-large')
        for species in SLED.keys():
            plt.errorbar(_np.arange(1,len(SLED[species])+1), SLED[species],yerr=SLEDerr[species],label=species)
        #### add plotting of the best-fit SLED model here (plotting sum and all
        #### components
        plt.legend(loc='upper right',
            scatterpoints=1,
            frameon=False)
        plt.yscale('log')
        plt.savefig(SLEDplot) 

    return fitparams

# Probability function for comparing the simulated and observed SLEDs
def SLEDprob(p,SLED,SLEDerr,TB,ncomp):
    # dictionary of standard abundances
    abundance={'co':1.0e-4,
        '13co': 5.0e-7,
        'hco+': 1.0e-8}
    lp=lnprior(p)
    if not _np.isfinite(lp):
        return -_np.inf
    nc=_cloud()
    compsleds={}
    # standard composition
    nc.comp.xoH2 = 0.1
    nc.comp.xpH2 = 0.4
    nc.comp.xHe = 0.1
    for spec in SLED.keys():
        nc.addEmitter(spec, abundance[spec]) # same emitters for all clouds
        compsleds[spec]=_np.zeros(len(SLED[spec]))
    for i in range(ncomp):
        amp,nc.nH,nc.colDen,nc.sigmaNT,nc.Tg=10**p[0+5*i:5+5*i]
        nc.Td=nc.Tg    # assume gas and dust have the same temperature
        for spec in SLED.keys():
            normed=False
            if (SLED[spec][0]-1)<1e-5:  # be forgiving of numerical effects?
                normed=True
            lines=nc.lineLum(spec)
            if TB:
                qty='intTB'
            else:
                qty='intIntensity'
            if normed:
                l=_np.array([l[qty] for l in lines])
                l=amp*l/l[0]
                compsleds[spec]+=l
            else:
                compsleds[spec]+=amp*_np.array([l[qty] for l in lines])

    sqerr=0
    # truncate compsled to the transitions we already have and compute difference
    for spec in SLED.keys():
        compsleds[spec]=compsleds[spec][:len(SLED[spec])]
        sqerr+=_np.nansum((SLED[spec]-compsleds[spec])**2*SLEDerr[spec]**-2+_np.log(2*_np.pi*SLEDerr[spec]**2))

    return (lp-0.5*sqerr)

# Logarithmic checking of priors
def lnprior(p):
    for i in range(len(p)/5):
        amp,nh,colDen,sigmaNT,Tg=p[0+5*i:5+5*i] # note these are log(param), not param
        if not(prange[1][0] < nh < prange[1][1]) or not(prange[2][0] < colDen < prange[2][1]) or not(prange[3][0] < sigmaNT < prange[3][1]) or not(prange[4][0] < Tg < prange[4][1]):
            return -_np.inf
    return 0.0
