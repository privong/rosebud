#!/usr/bin/env python

import numpy as np
import despotic

def COSLED(cloudprop,normed=False,TB=True,Int=False):
  """
  Return a CO SLED for some cloud properties.

  cloudprop: tuple/array of ( )
  normed: If true, normalize all  to CO(1-0)
  TB: If true (default: True) return ratios in brightness temperature
  Int: If true (default: False) return ratios of integrated intensities
  """
  if TB and Int:
    sys.stderr.write("Please pick only one of TB and Integrated Intensity")
    return [-1]
  if TB:
    key='intTB'
  elif Int:
    key='intIntensity'

  nc=despotic.cloud(
  nc.setTempEq(verbose=False)
  lines=nc.lineLum('co')
  if normed:
    nval=lines[0][key]
  else:
    nval=1

  SLED=[]
  for trans in range(len(lines)):
    SLED.append(lines[trans][key]/nval)

  return SLED
