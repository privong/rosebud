# IPython log file

#get_ipython().magic(u'logstart test_SLED.py')
import despotic
import rosebud
import numpy as np
#nc=despotic.cloud(fileName='/usr/lib/python2.7/site-packages/despotic/cloudfiles/ULIRG.desp')
# generate a ULIRG spectrum
nc=despotic.cloud(fileName='ULIRG-simple.desp')
a=nc.lineLum('co')
COs=np.array([l['intTB'] for l in a])
COsDict={'co':np.array(COs)}	

# generate a second CO SLED and add it to the first
nc.Tg=15    # cold component
nc.nH=100   # lower density
a=nc.lineLum('co')
COs=np.array([l['intTB'] for l in a])
COsDict['co']+=COs
COsErrDict={'co':COsDict['co']*0.01}    # assume everything was measured with 1%
                                        # accuracy
rosebud.rosebud(COsDict,COsErrDict,TB=True,VERBOSE=True,ncomp=2,nburn=50,nchain=4000,nthreads=4,nwalkers=80,SLEDplot='ULIRG_SLED2.png',plotfile='ULIRG_triangle2.png',truths=[0,5,24,6.9,1.65,0,2,24,6.9,1.0])
