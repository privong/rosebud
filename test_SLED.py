# IPython log file

#get_ipython().magic(u'logstart test_SLED.py')
import despotic
import rosebud
import numpy as np
#nc=despotic.cloud(fileName='/usr/lib/python2.7/site-packages/despotic/cloudfiles/ULIRG.desp')
nc=despotic.cloud(fileName='ULIRG-simple.desp')
a=nc.lineLum('co')
COs=np.array([l['intTB'] for l in a])
COsDict={'co':np.array(COs)/a[0]['intTB']}	# normalize SLED to 1--0 measurement
COsErrDict={'co':COsDict['co']*0.01}
rosebud.rosebud(COsDict,COsErrDict,TB=True,VERBOSE=True,nburn=10,nchain=100,nthreads=2,nwalkers=20,SLEDplot='ULIRG_SLED.png',plotfile='ULIRG_triangle.png',truths=[0,5,24,6.9,1.65])
